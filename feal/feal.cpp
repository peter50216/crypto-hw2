#include "feal.h"
#include "util.h"

#include <bits/stdc++.h>
#include <unistd.h>
using namespace std;

u32 keys[6];
vector<u64> inputs;

// Not-that-important stuffs. {{{

int verbose = 0;

void help_message(const string& prog_name) {
  cerr << "Usage: " << prog_name << " [options] [input 64bit numbers]..." << endl;
  cerr << endl;
  cerr << "Options:" << endl;
  cerr << "    -k [keyfile]: File contains subkeys. (required)" << endl;
  cerr << "    -v: verbose output" << endl;
  cerr << "    -h: help message (this)" << endl;
  exit(1);
}

void read_subkeys(const string& file) {
  INFO("Reading subkeys from " << file);
  ifstream f(file);
  if (f.fail()) {
    perror("read_subkeys");
    exit(1);
  }
  f >> setbase(0);
  for (int i = 0; i < 6; i++) {
    if (!(f >> keys[i])) ERR("keyfile " << file << " format error.");
  }
  f.close();
  INFO("Done reading subkeys");
}

void parse_options(int argc, char* argv[]) {
  char* key_file = NULL;
  int c;
  while ((c = getopt(argc, argv, "hk:v")) != -1) {
    switch (c) {
      case 'h':
        help_message(argv[0]);
        break;
      case 'k':
        key_file = optarg;
        break;
      case 'v':
        verbose += 1;
        break;
      case '?':
        if (c == 'k') ERR("Missing argument for -k");
        else ERR("Unknown option: -" << char(c));
      default:
        assert(false);
    }
  }
  if (key_file == NULL) {
    help_message(argv[0]);
    exit(1);
  }
  read_subkeys(key_file);
  for (int i = optind; i < argc; i++) {
    stringstream ss;
    u64 v;
    ss << argv[i];
    ss >> setbase(0) >> v;
    inputs.push_back(v);
  }
}

// }}}

int main(int argc, char* argv[]) {
  parse_options(argc, argv);
  for (u64 x : inputs) {
    cout << formatted_hex(16) << encrypt(x, keys) << endl;
  }
}

// vim: fdm=marker:commentstring=\ \"\ %s:nowrap:autoread
