#pragma once
#include <iostream>
#include <iomanip>

extern int verbose;

typedef uint8_t u8;
typedef uint32_t u32;
typedef uint64_t u64;
typedef unsigned int uint;

#define INFO(...) do {\
  if (verbose >= 1) std::cerr << __VA_ARGS__ << std::endl;\
} while(0)
#define ERR(...) do {std::cerr << "In " << __PRETTY_FUNCTION__ << ":" << endl\
  << "    " << __VA_ARGS__ << std::endl; exit(1);} while(0)

#ifndef NDEBUG
#define ASSERT(x, ...) if (!(x)) ERR(__VA_ARGS__)
#define DEBUG(...) do {\
  if (verbose >= 2) std::cerr << __VA_ARGS__ << std::endl;\
} while(0)
#else
#define ASSERT(x, ...)
#define DEBUG(...)
#endif

struct formatted_hex {
  unsigned int n;
  explicit formatted_hex(unsigned int in): n(in) {}
};

std::ostream& operator<<(std::ostream& out, const formatted_hex& fh) {
  return out << "0x" << std::hex << std::setw(fh.n) << std::setfill('0');
}

std::ostream& phex(std::ostream& out) {
  return out << "0x" << std::hex;
}

bool get_bit(u64 x, uint i) { return (x >> i) & 1; }
