#pragma once
#include <cstdint>
#include <algorithm>

typedef uint8_t u8;
typedef uint32_t u32;
typedef uint64_t u64;

inline u8 get_byte(u32 x, int i) { return x >> (8 * i); }
inline u32 combine_bytes(u8 x0, u8 x1, u8 x2, u8 x3) {
  return (u32(x0) << 24) | (u32(x1) << 16) | (u32(x2) << 8) | x3;
}

inline u8 rotl2(u8 x) { return (x << 2) | (x >> 6); }
inline u32 g0(u8 a, u8 b) { return rotl2(a+b); }
inline u32 g1(u8 a, u8 b) { return rotl2(a+b+1); }
inline u32 fbox(u32 x) {
  u8 x0 = get_byte(x, 3);
  u8 x1 = get_byte(x, 2);
  u8 x2 = get_byte(x, 1);
  u8 x3 = get_byte(x, 0);
  x1 ^= x0;
  x2 ^= x3;
  x1 = g1(x1, x2);
  x2 = g0(x1, x2);
  x0 = g0(x0, x1);
  x3 = g1(x2, x3);
  return combine_bytes(x0, x1, x2, x3);
}

inline u32 left_half(u64 x) { return x >> 32; }
inline u32 right_half(u64 x) { return x; }
inline u64 combine_halves(u32 l, u32 r) {
  return (u64(l) << 32) | u64(r);
}

u64 encrypt(u64 x, u32 keys[6]) {
  u32 l = left_half(x);
  u32 r = right_half(x);
  l ^= keys[4];
  r ^= keys[5];
  r ^= l;
  for (int i = 0; i < 4; i++) {
    l ^= fbox(r ^ keys[i]);
    if (i < 3) std::swap(l, r);
  }
  r ^= l;
  return combine_halves(l, r);
}
