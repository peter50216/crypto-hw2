#include "feal.h"
#include "util.h"

#include <bits/stdc++.h>
#include <unistd.h>
using namespace std;

string blackbox_cmd;
int num_pair_round = 4;

// Not-that-important stuffs. {{{

int verbose = 0;

void help_message(const string& prog_name) {
  cerr << "Usage: " << prog_name << " [options] [blackbox executable]" << endl;
  cerr << endl;
  cerr << "Options:" << endl;
  cerr << "    -v: verbose output" << endl;
  cerr << "    -h: help message (this)" << endl;
  cerr << "    -n [num]: set per-round plain-text pair used (default 4)" << endl;
  cerr << "              would use more if unlucky :D" << endl;
  cerr << endl;
  cerr << "Blackbox should be able to be executed as:" << endl;
  cerr << "  blackbox [input 64bit integer]" << endl;
  cerr << "  and output the encrypted result to stdout." << endl;
  exit(1);
}

void parse_options(int argc, char* argv[]) {
  int c;
  while ((c = getopt(argc, argv, "hn:v")) != -1) {
    switch (c) {
      case 'h':
        help_message(argv[0]);
        break;
      case 'n':
        num_pair_round = atoi(optarg);
        break;
      case 'v':
        verbose += 1;
        break;
      case '?':
        if (c == 'n') ERR("Missing argument for -n");
        else ERR("Unknown option: -" << char(c));
      default:
        assert(false);
    }
  }
  if (argc - optind != 1) {
    cerr << "Incorrect number of arguments." << endl;
    help_message(argv[0]);
    exit(1);
  }
  blackbox_cmd = argv[optind];
}

// }}}

default_random_engine generator(random_device{}());
uniform_int_distribution<u64> distribution(0, numeric_limits<u64>::max());
auto rand_u64 = bind(distribution, generator);

vector<pair<u64, u64>> all_io;

u64 blackbox(u64 x) {
  stringstream ss;
  ss << blackbox_cmd;
  ss << " " << formatted_hex(16) << x;
  ss << " > tmp.out";
  if (system(ss.str().c_str()) != 0) ERR("Error when executing blackbox.");

  ifstream ifs("tmp.out");
  if (ifs.fail()) ERR("Blackbox has no output.");
  u64 out;
  ifs >> setbase(0);
  if (!(ifs >> out)) ERR("Error when reading blackbox output.");
  ifs.close();
  all_io.emplace_back(x, out);
  return out;
}

// left & right parts of output separated.
typedef pair<u32, u32> out_type;
// We never use inputs of diff_pairs
vector<pair<out_type, out_type>> diff_pairs;

pair<out_type, out_type> gen_diff_pair(u64 diff) {
  u64 in0 = rand_u64();
  u64 in1 = in0 ^ diff;
  u64 out0 = blackbox(in0), out1 = blackbox(in1);
  return {{left_half(out0), right_half(out0)},
    {left_half(out1), right_half(out1)}};
}

void gen_diff_pairs(u64 diff, int cnt) {
  diff_pairs.clear();
  for (int i = 0; i < cnt; i++) diff_pairs.push_back(gen_diff_pair(diff));
}

u32 found_keys[6];
const u64 in_diff[4] = // [0] is not used.
{0, 0x0000000002000000LL, 0x0000000080800000LL, 0x8080000080800000LL};
const u32 out_diff = 0x02000000L;

void undo_last_step(out_type& x) {
  x.second ^= x.first;
}

void undo_last_step() {
  for (auto& p : diff_pairs) {
    undo_last_step(p.first);
    undo_last_step(p.second);
  }
}

void undo_round(u32 key, out_type& x) {
  x.first ^= fbox(x.second ^ key);
  swap(x.first, x.second);
}

void undo_round(u32 key) {
  for (auto& p : diff_pairs) {
    undo_round(key, p.first);
    undo_round(key, p.second);
  }
}

typedef out_type in_type;
typedef pair<in_type, out_type> io_pair;

pair<u32, u32> undo_first_round(u32 k, const io_pair& p) {
  u32 l, r;
  tie(l, r) = p.second;
  l ^= fbox(r ^ k);
  r ^= l;
  return {l ^ p.first.first, r ^ p.first.second};
}

int main(int argc, char* argv[]) {
  parse_options(argc, argv);
  for (int r = 3; r > 0; r--) {
    INFO("Cracking subkey for round " << r);
    gen_diff_pairs(in_diff[r], num_pair_round);
    undo_last_step();
    for (int rr = 3; rr > r; rr--) undo_round(found_keys[rr]);

    INFO("Undo done, guessing subkey...");
    vector<u32> pos_keys;
    // Hack around openmp limits.
    for (int kk0 = 0; kk0 < (1 << 4); kk0++) {
      INFO("Progress: " << formatted_hex(8) << (kk0 << 27) << "/0x80000000");
#pragma omp parallel for
      for (int kk1 = 0; kk1 < (1 << 27); kk1++) {
        u32 k = (kk0 << 27) | kk1;

        // Since the f-box have sooo good differential charateristic,
        // there are some equivalent subkey set. So these keys can be skipped.
        // (k ^ 0x80800000, k ^ 0x00008080) would also be correct subkey.
        if (k & 0x80008000) continue;

        bool correct = true;

        for (auto& p : diff_pairs) {
          out_type& out0 = p.first;
          out_type& out1 = p.second;
          u32 v0 = out0.first ^ fbox(out0.second ^ k);
          u32 v1 = out1.first ^ fbox(out1.second ^ k);
          if ((v0 ^ v1) != out_diff) {
            correct = false;
            break;
          }
        }

        if (correct) {
#pragma omp critical
          pos_keys.push_back(k);
        }
      }
    }

    int rounds = 0;
    while (pos_keys.size() > 1 && rounds <= 100) {
      INFO("There are " << dec << pos_keys.size() << " possible keys left."
          << " Use more diff pairs to find correct ones.");
      auto p = gen_diff_pair(in_diff[r]);
      undo_last_step(p.first);
      undo_last_step(p.second);
      for (int rr = 3; rr > r; rr--) {
        undo_round(found_keys[rr], p.first);
        undo_round(found_keys[rr], p.second);
      }

      vector<u32> new_pos_keys;
      for (u32 k : pos_keys) {
        out_type& out0 = p.first;
        out_type& out1 = p.second;
        u32 v0 = out0.first ^ fbox(out0.second ^ k);
        u32 v1 = out1.first ^ fbox(out1.second ^ k);
        if ((v0 ^ v1) == out_diff) new_pos_keys.push_back(k);
      }
      pos_keys = new_pos_keys;
      rounds++;
    }

    if (pos_keys.empty()) {
      ERR("Can't find possible subkey for round " << r << endl
          << "  Is the blackbox really a feal-4 cipher???");
    } else if (pos_keys.size() > 1) {
      ERR("Too many possible subkey for round " << r << endl
          << "  Is the blackbox really a feal-4 cipher???");
    } else {
      u32 k = pos_keys[0];
      INFO("Found key for round " << r << " = " << formatted_hex(8) << k);
      found_keys[r] = k;
    }
  }

  INFO("Cracking subkey for round 0");
  vector<io_pair> vals;
  // TODO: fixing this to 16 now.
  for (int i = 0; i < 16; i++) {
    u64 x = rand_u64();
    u64 o = blackbox(x);
    io_pair p{{left_half(x), right_half(x)}, {left_half(o), right_half(o)}};
    undo_last_step(p.second);
    for (int r = 3; r > 0; r--) undo_round(found_keys[r], p.second);
    vals.push_back(p);
  }

  INFO("Undo done, guessing subkey...");
  vector<pair<u32, pair<u32, u32>>> pos_keys;
  // Hack around openmp limits.
  for (int kk0 = 0; kk0 < (1 << 4); kk0++) {
    INFO("Progress: " << formatted_hex(8) << (kk0 << 27) << "/0x80000000");
#pragma omp parallel for
    for (int kk1 = 0; kk1 < (1 << 27); kk1++) {
      u32 k = (u32(kk0) << 27) | kk1;
      // Since the f-box have sooo good differential charateristic,
      // there are some equivalent subkey set. So these keys can be skipped.
      // (k ^ 0x80800000, k ^ 0x00008080) would also be correct subkey.
      if (k & 0x80008000) continue;

      auto k45 = undo_first_round(k, vals[0]);
      bool correct = true;
      for (size_t i = 1; i < vals.size(); i++) {
        auto ck = undo_first_round(k, vals[i]);
        if (ck != k45) {
          correct = false;
          break;
        }
      }

      if (correct) {
#pragma omp critical
        pos_keys.emplace_back(k, k45);
      }
    }
  }

  if (pos_keys.size() == 1) {
    u32 k;
    pair<u32, u32> k45;
    tie(k, k45) = pos_keys[0];
    INFO("Found key for round 0 = " << formatted_hex(8) << k);
    found_keys[0] = k;
    tie(found_keys[4], found_keys[5]) = k45;
  } else if (pos_keys.empty()) {
    ERR("Can't find possible subkey for round 0" << endl
        << "  Is the blackbox really a feal-4 cipher???");
  } else {
    ERR("Too many possible subkey for round 0" << endl
        << "  Is the blackbox really a feal-4 cipher???");
  }

  INFO("Verifying the subkeys found...");
  for (auto& p : all_io) {
    if (p.second != encrypt(p.first, found_keys)) {
      ERR("Found key wrong?! input = " << formatted_hex(16) << p.first
          << ", output = " << formatted_hex(16) << encrypt(p.first, found_keys) << endl
          << "  But answer is " << p.second);
    }
  }

  INFO("Verify done! Here's the keys:");
  for (int i = 0; i < 6; i++) cout << formatted_hex(8) << found_keys[i] << endl;
}
// vim: fdm=marker:commentstring=\ \"\ %s:nowrap:autoread
