#pragma once
#include "util.h"
#include <bits/stdc++.h>
#include "json/json.h"

using namespace std;

struct SBox {
  uint bits;
  vector<u32> s, sinv;
  SBox() : bits(0) {}
  SBox(uint _bits, const vector<u32>& _s) : bits(_bits), s(_s) {
    ASSERT(bits <= 32, "sbox_bits too large.");
    ASSERT(s.size() == (1u << bits), "sbox_bits doesn't match sbox.");

    sinv.resize(s.size(), s.size()); // Using s.size() value as undefined.
    for (size_t i = 0; i < s.size(); i++) {
      ASSERT (s[i] < s.size(), "value " << phex << s[i] << " out of range in sbox.");
      ASSERT (sinv[s[i]] == s.size(),
          "value " << phex << s[i] << " duplicated in sbox.");
      sinv[s[i]] = i;
    }
  }
  u32 operator[](u32 x) const { return s[x]; }
  u32 max_input() const { return (1u << bits) - 1; }
};

struct PBox {
  uint bits;
  vector<u32> p, pinv;
  PBox() : bits(0) {}
  PBox(uint _bits, const vector<u32>& _p) : bits(_bits), p(_p) {
    ASSERT(bits <= 64, "total_bits too large.");
    ASSERT(p.size() == bits, "total_bits doesn't match pbox.");

    pinv.resize(p.size(), p.size()); // Using p.size() value as undefined.
    for (size_t i = 0; i < p.size(); i++) {
      ASSERT(p[i] < p.size(), "value " << phex << p[i] << " out of range in pbox.");
      ASSERT(pinv[p[i]] == p.size(),
          "value " << phex << p[i] << " duplicated in pbox.");
      pinv[p[i]] = i;
    }
  }
  u32 operator[](u32 x) const {return p[x];}
};

uint getUInt(const Json::Value& v, const string& k) {
  ASSERT(v[k].isUInt(), k << " should be an unsigned int.");
  return v[k].asUInt();
}

vector<u32> getU32Array(const Json::Value& v, const string& k) {
  vector<u32> ret;
  const Json::Value& vk = v[k];
  ASSERT(vk.isArray(), k << " should be an array.");
  for (Json::ArrayIndex i = 0; i < vk.size(); i++) {
    ASSERT(vk[i].isUInt(), "Elements of " << k << " should be unsigned ints.");
    ret.push_back(vk[i].asUInt());
  }
  return ret;
}

u64 bit_permutation(u64 x, const vector<u32>& p) {
  u64 nx = 0;
  // order reverse of figure on ppt.
  for (uint i = 0; i < p.size(); i++) {
    if (get_bit(x, i)) nx |= 1ull << p[i];
  }
  return nx;
}

u64 sub_each_block(u64 x, const vector<u32>& s, u32 sbits, u32 num_block) {
  u64 nx = 0;
  for (uint i = 0; i < num_block; i++) {
    nx |= u64(s[get_block(x, i, sbits)]) << (i * sbits);
  }
  return nx;
}

struct SPN {
  SBox sbox;
  PBox pbox;
  uint rounds = 0;
  uint total_bits;
  uint block_cnt;

  void from_file(const string& filename) {
    INFO("Reading SPN from file " << filename);
    ifstream ifs(filename);
    if (ifs.fail()) ERR("Can't open file " << filename);

    Json::Reader reader;
    Json::Value root;
    if (!reader.parse(ifs, root)) ERR("Json parse error in file " << filename);

    rounds = getUInt(root, "rounds");

    total_bits = getUInt(root, "total_bits");
    ASSERT(total_bits <= 64, "total_bits too large.");

    uint sbox_bits = getUInt(root, "sbox_bits");
    ASSERT(sbox_bits > 0 && total_bits % sbox_bits == 0,
        "total_bits should be divisible by sbox_bits");

    block_cnt = total_bits / sbox_bits;

    sbox = SBox(sbox_bits, getU32Array(root, "sbox"));
    pbox = PBox(total_bits, getU32Array(root, "pbox"));
  }

  u64 max_input() const { return (1ull << total_bits) - 1; }

  vector<u64> read_keys(const string& filename) const {
    INFO("Reading SPN keys from file " << filename);
    ifstream ifs(filename);
    if (ifs.fail()) ERR("Can't open file " << filename);
    ifs >> setbase(0);

    vector<u64> keys;
    for (uint i = 0; i <= rounds; i++) {
      u64 k;
      if (!(ifs >> k)) ERR("Error when reading file " << filename);
      ASSERT(k <= max_input(), "Key have too many bits.");
      keys.push_back(k);
    }
    return keys;
  }

  u64 encrypt(u64 x, const vector<u64>& keys) const {
    ASSERT(x <= max_input(), "Input too large.");
    ASSERT(keys.size() == rounds + 1, "Number of keys doesn't match rounds.");
    for (u64 k : keys) {
      ASSERT(k <= max_input(), "Key " << phex << k << "have too many bits.");
    }
    for (uint r = 0; r < rounds; r++) {
      DEBUG("Encrypt round " << r << " x = " << phex << x);
      x ^= keys[r];
      DEBUG("After xor key: " << phex << x);
      x = sub_each_block(x, sbox.s, sbox.bits, block_cnt);
      DEBUG("After sbox: " << phex << x);
      if (r + 1 != rounds) { // Not last round, permute.
        x = bit_permutation(x, pbox.p);
      }
    }
    return x ^ keys[rounds];
  }
};
