#include "json/json.h"
#include "spn.h"
#include "util.h"

#include <bits/stdc++.h>
#include <unistd.h>

using namespace std;
using namespace Json;

vector<u64> inputs;
SPN spn;
vector<u64> keys;

// Not-that-important stuffs. {{{

int verbose = 0;

void help_message(const string& prog_name) {
  cerr << "Usage: " << prog_name << " [options] [input numbers]..." << endl;
  cerr << "If input numbers are empty, then read input numbers from stdin." << endl;
  cerr << endl;
  cerr << "Options:" << endl;
  cerr << "    -c [SPN json]: File contains description of SPN. (required)" << endl;
  cerr << "    -k [keyfile]: File contains subkeys. (required)" << endl;
  cerr << "    -v: verbose output" << endl;
  cerr << "    -h: help message (this)" << endl;
  exit(1);
}

void parse_options(int argc, char* argv[]) {
  char* key_file = NULL;
  char* spn_file = NULL;
  int c;
  while ((c = getopt(argc, argv, "hc:k:v")) != -1) {
    switch (c) {
      case 'h':
        help_message(argv[0]);
        break;
      case 'c':
        spn_file = optarg;
        break;
      case 'k':
        key_file = optarg;
        break;
      case 'v':
        verbose += 1;
        break;
      case '?':
        if (c == 'k') ERR("Missing argument for -k");
        if (c == 'c') ERR("Missing argument for -c");
        else ERR("Unknown option: -" << char(c));
      default:
        assert(false);
    }
  }
  if (key_file == NULL || spn_file == NULL) {
    help_message(argv[0]);
    exit(1);
  }

  spn.from_file(spn_file);
  keys = spn.read_keys(key_file);

  for (int i = optind; i < argc; i++) {
    stringstream ss;
    u64 v;
    ss << argv[i];
    ss >> setbase(0) >> v;
    ASSERT(v <= spn.max_input(), "input " << phex << v << "have too many bits");
    inputs.push_back(v);
  }
}

// }}}

int main(int argc, char* argv[]) {
  parse_options(argc, argv);
  if (inputs.empty()) {
    cin >> setbase(0);
    u64 x;
    while (cin >> x) {
      cout << formatted_hex(spn.total_bits, true) << spn.encrypt(x, keys) << endl;
    }
  } else {
    for (u64 x : inputs) {
      cout << formatted_hex(spn.total_bits, true) << spn.encrypt(x, keys) << endl;
    }
  }
}
// vim: fdm=marker:commentstring=\ \"\ %s:nowrap:autoread
