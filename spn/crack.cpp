#include "util.h"
#include "spn.h"

#include <bits/stdc++.h>
#include <unistd.h>
using namespace std;

string blackbox_cmd;
SPN spn;
double min_linear_bias = 0.25;
double min_path_linear_bias = 0.04;
double linear_coef = 20;

double min_diff_prob = 0.25;
double min_path_diff_prob = 0.01;
double diff_coef = 4;

const double eps = 1e-8;

uint num_io_r1 = 30;
uint num_cover = 4;

default_random_engine generator(random_device{}());
uniform_int_distribution<u64> distribution;
u64 rand_input() { return distribution(generator); }

// Not-that-important stuffs. {{{

int verbose = 0;

void help_message(const string& prog_name) {
  cerr << "Usage: " << prog_name << " [options] [blackbox executable]" << endl;
  cerr << endl;
  cerr << "Options:" << endl;
  cerr << "    -c [SPN json]: File contains description of SPN. (required)" << endl;
  cerr << "    -v: verbose output" << endl;
  cerr << "    -h: help message (this)" << endl;
  cerr << "    -r: number of times a key guess being maximum to be considered" << endl;
  cerr << "        correct. (to make things more robust.) (default 4)" << endl;
  cerr << "  Linear Cryptanalysis:" << endl;
  cerr << "    -l [double]: minimum linear bias for an item in" << endl;
  cerr << "                 linear approximation table to be considered" << endl;
  cerr << "                 (default 0.25)" << endl;
  cerr << "    -L [double]: minimum linear bias for a path" << endl;
  cerr << "                 (default 0.04)" << endl;
  cerr << "    -q [double]: number of query to blackbox would be q*b^{-2}" << endl;
  cerr << "                 where b is bias. (default: 20)";
  cerr << "  Differential Cryptanalysis:" << endl;
  cerr << "    -d [double]: minimum probability for an item in" << endl;
  cerr << "                 difference distribution table to be considered" << endl;
  cerr << "                 (default 0.25)" << endl;
  cerr << "    -D [double]: minimum probability for a path" << endl;
  cerr << "                 (default 0.01)" << endl;
  cerr << "    -w [double]: number of pairs to blackbox would be w*p^{-2}" << endl;
  cerr << "                 where p is probability of path. (default: 4)";
  cerr << endl;
  cerr << "    -k [int]: number of query to blackbox for round 1. (default 30)" << endl;
  cerr << endl;
  cerr << "Blackbox should be able to read 64 bit integers from stdin" << endl;
  cerr << "  and output the encrypted result to stdout." << endl;
  exit(1);
}

void parse_options(int argc, char* argv[]) {
  char* spn_file = NULL;
  int c;
  while ((c = getopt(argc, argv, "hc:vl:L:q:d:D:w:k:")) != -1) {
    switch (c) {
      case 'h':
        help_message(argv[0]);
        break;
      case 'c':
        spn_file = optarg;
        break;
      case 'v':
        verbose += 1;
        break;
      case 'l':
        sscanf(optarg, "%lf", &min_linear_bias);
        break;
      case 'L':
        sscanf(optarg, "%lf", &min_path_linear_bias);
        break;
      case 'q':
        sscanf(optarg, "%lf", &linear_coef);
        break;
      case 'd':
        sscanf(optarg, "%lf", &min_diff_prob);
        break;
      case 'D':
        sscanf(optarg, "%lf", &min_path_diff_prob);
        break;
      case 'w':
        sscanf(optarg, "%lf", &diff_coef);
        break;
      case 'k':
        sscanf(optarg, "%u", &num_io_r1);
        break;
      case '?':
        exit(1);
      default:
        assert(false);
    }
  }
  if (argc - optind != 1) {
    cerr << "Incorrect number of arguments." << endl;
    help_message(argv[0]);
    exit(1);
  }
  if (spn_file == NULL) {
    help_message(argv[0]);
    exit(1);
  }

  blackbox_cmd = argv[optind];
  spn.from_file(spn_file);

  distribution = uniform_int_distribution<u64>(0, spn.max_input());
}

// }}}

vector<pair<u64, u64>> all_io;

vector<u64> blackbox(const vector<u64>& xv) {
  ofstream ofs("tmp.in");
  ASSERT(!ofs.fail(), "tmp.in has to be writable.");
  for (u64 x : xv) ofs << formatted_hex(spn.total_bits, true) << x << endl;
  ofs.close();
  stringstream ss;
  ss << blackbox_cmd;
  ss << " < tmp.in > tmp.out";
  ASSERT(system(ss.str().c_str()) == 0, "Error when executing blackbox.");

  ifstream ifs("tmp.out");
  ASSERT(!ifs.fail(), "Blackbox has no output.");
  vector<u64> out;
  ifs >> setbase(0);
  for (size_t i = 0; i < xv.size(); i++) {
    u64 o;
    ASSERT(ifs >> o, "Error when reading blackbox output.");
    ASSERT(o <= spn.max_input(),
        "Blackbox output " << phex << o << " is too large.");
    all_io.emplace_back(xv[i], o);
    out.push_back(o);
  }
  ifs.close();

  return out;
}

vector<pair<u64, u64>> rand_pool;
vector<pair<u64, u64>> rand_iop(uint size, u64 diff = 0) {
  vector<u64> todo;
  if (size > rand_pool.size()) {
    todo.reserve(size - rand_pool.size());
    while (rand_pool.size() + todo.size() < size) {
      todo.push_back(rand_input());
    }
    vector<u64> output = blackbox(todo);
    rand_pool.reserve(size);
    for (size_t i = 0; i < output.size(); i++) {
      rand_pool.emplace_back(todo[i], output[i]);
    }
  }
  //random_shuffle(rand_pool.begin(), rand_pool.end());
  vector<pair<u64, u64>> ret;
  if (diff != 0) {
    ret.reserve(2 * size);
    todo.clear();
    todo.reserve(size);
    for (uint i = 0; i < size; i++) {
      todo.push_back(rand_pool[i].first ^ diff);
    }
    vector<u64> output = blackbox(todo);
    for (uint i = 0; i < size; i++) {
      ret.push_back(rand_pool[i]);
      ret.emplace_back(todo[i], output[i]);
    }
  } else {
    ret.reserve(size);
    for (uint i = 0; i < size; i++) {
      ret.push_back(rand_pool[i]);
    }
  }
  return ret;
}

vector<vector<double>> linear_tbl;
vector<vector<u32>> linear_cands;
set<u32> bad_linear_block;

void gen_linear_table() {
  INFO("Generating linear approximation table.");

  u32 maxv = spn.sbox.max_input();
  ASSERT(maxv <= 4096, "SBox too large for attack :(");

  linear_tbl.resize(maxv + 1);
  for (size_t i = 0; i < linear_tbl.size(); i++) {
    linear_tbl[i].resize(maxv + 1);
  }

#pragma omp parallel for
  for (int ib = 0; ib < (int)(maxv + 1); ib++) {
    for (u32 ob = 0; ob <= maxv; ob++) {
      for (u32 x = 0; x <= maxv; x++) {
        u32 y = spn.sbox[x];
        if ((__builtin_popcount(x & ib) + __builtin_popcount(y & ob)) % 2 == 0) {
          linear_tbl[ib][ob] += 1;
        }
      }
    }
  }

  // scale bias, so Piling-up lemma is in much simpler form.
  min_linear_bias *= 2;
  min_path_linear_bias *= 2;
  linear_coef *= 4;

  linear_cands.resize(maxv + 1);
  for (u32 ib = 0; ib <= maxv; ib++) {
    for (u32 ob = 0; ob <= maxv; ob++) {
      linear_tbl[ib][ob] = 2 * abs(linear_tbl[ib][ob] - (maxv + 1) / 2) / (maxv + 1);
      if (linear_tbl[ib][ob] >= min_linear_bias - eps) {
        INFO("Candidate for linear attack found: "
            << phex << ib << " -> " << phex << ob
            << " = " << dec << linear_tbl[ib][ob]);
        linear_cands[ib].push_back(ob);
      }
    }
  }
}

vector<vector<double>> diff_tbl;
vector<vector<u32>> diff_cands;
set<u32> bad_diff_block;

void gen_diff_table() {
  INFO("Generating difference distribution table.");

  u32 maxv = spn.sbox.max_input();
  ASSERT(maxv <= 4096, "SBox too large for attack :(");

  diff_tbl.resize(maxv + 1);
  for (size_t i = 0; i < diff_tbl.size(); i++) {
    diff_tbl[i].resize(maxv + 1);
  }

#pragma omp parallel for
  for (int id = 0; id < (int)(maxv + 1); id++) {
    for (u32 x0 = 0; x0 <= maxv; x0++) {
      u32 x1 = x0 ^ u32(id);
      u32 y0 = spn.sbox[x0], y1 = spn.sbox[x1];
      diff_tbl[id][y0 ^ y1] += 1;
    }
  }

  diff_cands.resize(maxv + 1);
  for (u32 ib = 0; ib <= maxv; ib++) {
    for (u32 ob = 0; ob <= maxv; ob++) {
      diff_tbl[ib][ob] /= maxv + 1;
      if (diff_tbl[ib][ob] >= min_diff_prob - eps) {
        INFO("Candidate for diff attack found: "
            << phex << ib << " -> " << phex << ob
            << " = " << dec << diff_tbl[ib][ob]);
        diff_cands[ib].push_back(ob);
      }
    }
  }
}

void gen_bad_blocks() {
  u32 maxv = spn.sbox.max_input();
  for (u32 a = 1; a <= maxv; a++) {
    for (u32 b = 1; b <= maxv; b++) {
      double c0 = 0, c1 = 0;
      for (u32 c = 1; c <= maxv; c++) {
        if (__builtin_popcount(a & c) & 1) {
          c1 += diff_tbl[c][b];
        } else {
          c0 += diff_tbl[c][b];
        }
      }
      if (c0 == 0 || c1 == 0) {
        // The a is bad, since if we only see bits of a,
        // then for ANY value x,
        // BITXOR(sbox_inv[x] & a) and BITXOR(sbox_inv[x ^ b] & a)
        // are always equal or always non-equal.
        // This means that we can't distinguish from key and key^b...
        INFO("Found too-linear block value: "
            << formatted_hex(spn.sbox.bits, true) << a
            << ", b = " << formatted_hex(spn.sbox.bits, true) << b);
        bad_linear_block.insert(a);
        break;
      }
    }
  }
}

vector<u64> found_keys;

void reverse(int r, u64& out) {
  for (int i = spn.rounds; i > r; i--) {
    out ^= found_keys[i];
    if (i != int(spn.rounds)) {
      out = bit_permutation(out, spn.pbox.pinv);
    }
    out = sub_each_block(out, spn.sbox.sinv, spn.sbox.bits, spn.block_cnt);
  }
  if (r != int(spn.rounds)) out = bit_permutation(out, spn.pbox.pinv);
}

typedef pair<pair<u64, u64>, double> path_type;
vector<path_type> paths;
void dfs(uint r, uint b, uint total_r,
    u64 first_v, u64 cur_v, double cur_p, double min_p,
    const vector<vector<double>>& p, const vector<vector<u32>> cand,
    const set<u32>& bad_block) {
  //if (paths.size() >= 100000) return;
  if (r == total_r) {
    // XXX: we want to find one that is not that biased on last block (?)
    for (uint i = 0; i < spn.block_cnt; i++) {
      if (bad_block.count(get_block(cur_v, i, spn.sbox.bits))) return;
    }
    DEBUG("Found a path: " << formatted_hex(spn.total_bits, true) << first_v
        << " -> " << formatted_hex(spn.total_bits, true) << cur_v
        << ", val = " << cur_p);
    paths.push_back(path_type{{first_v, cur_v}, cur_p});
    return;
  }
  if (b == spn.block_cnt) {
    if (r == 0 && first_v == 0) return;
    cur_v = bit_permutation(cur_v, spn.pbox.p);
    dfs(r + 1, 0, total_r, first_v, cur_v, cur_p, min_p, p, cand, bad_block);
    return;
  }
  u32 rangel, ranger;
  if (r == 0) { // try everything.
    rangel = 0;
    ranger = spn.sbox.max_input();
  } else {
    rangel = ranger = get_block(cur_v, b, spn.sbox.bits);
  }
  for (u32 x = rangel; x <= ranger; x++) {
    for (u32 nx : cand[x]) {
      if (cur_p * p[x][nx] < min_p) continue;
      u64 nf = r == 0 ? (first_v ^ (x << (b * spn.sbox.bits))) : first_v;
      u64 nc = cur_v ^ ((r == 0 ? nx : (x ^ nx)) << (b * spn.sbox.bits));
      dfs(r, b + 1, total_r, nf, nc, cur_p * p[x][nx], min_p, p, cand, bad_block);
    }
  }
}

vector<bool> block_done;
vector<u32> key_block_val;
vector<map<u32, uint>> key_block_val_cnt;

vector<u32> cur_key_block_val;
vector<u32> best_key_block_val;
double max_bias;

void dfs_linear_key(uint b, u64 start_v, u64 end_v,
    const vector<pair<u64, u64>>& io_pair) {
  if (b == spn.block_cnt) {
    double cnt = 0;
    for (auto& p : io_pair) {
      int bc = __builtin_popcountll(p.first & start_v);
      for (size_t i = 0; i < spn.block_cnt; i++) {
        u32 mask = get_block(end_v, i, spn.sbox.bits);
        if (mask) {
          u32 oblock = get_block(p.second, i, spn.sbox.bits);
          bc += __builtin_popcount(
              spn.sbox.sinv[oblock ^ cur_key_block_val[i]] & mask);
        }
      }
      if (bc & 1) cnt++;
    }
    cnt = fabs(cnt - io_pair.size() / 2.0) / io_pair.size();
    if (cnt >= max_bias) {
      max_bias = cnt;
      best_key_block_val = cur_key_block_val;
    }
    return;
  }
  if (block_done[b]) {
    cur_key_block_val[b] = key_block_val[b];
    dfs_linear_key(b + 1, start_v, end_v, io_pair);
  } else {
    u32 block = get_block(end_v, b, spn.sbox.bits);
    if (block == 0) {
      cur_key_block_val[b] = 0;
      dfs_linear_key(b + 1, start_v, end_v, io_pair);
    } else {
      for (u32 k = 0; k <= spn.sbox.max_input(); k++) {
        cur_key_block_val[b] = k;
        dfs_linear_key(b + 1, start_v, end_v, io_pair);
      }
    }
  }
}

double max_prob;

void dfs_diff_key(uint b, u64 end_diff,
    const vector<pair<u64, u64>>& o_pair) {
  if (b == spn.block_cnt) {
    double cnt = 0;
    for (auto& p : o_pair) {
      bool good = true;
      for (size_t i = 0; i < spn.block_cnt; i++) {
        u32 diff = get_block(end_diff, i, spn.sbox.bits);
        if (diff) {
          u32 ob0 = get_block(p.first, i, spn.sbox.bits);
          u32 ob1 = get_block(p.second, i, spn.sbox.bits);
          if ((spn.sbox.sinv[ob0 ^ cur_key_block_val[i]] ^
              spn.sbox.sinv[ob1 ^ cur_key_block_val[i]]) != diff) {
            good = false; break;
          }
        }
      }
      if (good) cnt++;
    }
    if (cnt >= max_prob) {
      max_prob = cnt;
      best_key_block_val = cur_key_block_val;
    }
    return;
  }
  if (block_done[b]) {
    cur_key_block_val[b] = key_block_val[b];
    dfs_diff_key(b + 1, end_diff, o_pair);
  } else {
    u32 block = get_block(end_diff, b, spn.sbox.bits);
    if (block == 0) {
      cur_key_block_val[b] = 0;
      dfs_diff_key(b + 1, end_diff, o_pair);
    } else {
      for (u32 k = 0; k <= spn.sbox.max_input(); k++) {
        cur_key_block_val[b] = k;
        dfs_diff_key(b + 1, end_diff, o_pair);
      }
    }
  }
}

bool cmp_path(const path_type& a, const path_type& b) {
  return a.second > b.second;
}

void linear_attack(int r) {
  if (all_of(block_done.begin(), block_done.end(), [](bool x){return x;})) {
    INFO("All keys already found, skipping linear attack.");
    return;
  }
  INFO("Trying linear attack on round " << r);
  INFO("Finding high bias paths.");
  paths.clear();
  dfs(0, 0, r - 1, 0, 0, 1, min_path_linear_bias,
      linear_tbl, linear_cands, bad_linear_block);
  INFO(dec << paths.size() << " paths found.");
  sort(paths.begin(), paths.end(), cmp_path);
  for (auto& p : paths) {
    bool flag = false;
    for (size_t i = 0; i < spn.block_cnt; i++) {
      if (get_block(p.first.second, i, spn.sbox.bits) != 0) {
        if (!block_done[i]) flag = true;
      }
    }
    if (!flag) continue;
    uint num = linear_coef / p.second / p.second + 1;
    INFO("Using path " << formatted_hex(spn.total_bits, true) << p.first.first
        << " -> " << formatted_hex(spn.total_bits, true) << p.first.second
        << " with bias " << p.second / 2 << endl
        << "  Would choose " << dec << num << " plaintexts.");
    vector<pair<u64, u64>> io_pair = rand_iop(num);
    for (auto& p : io_pair) reverse(r, p.second);
    cur_key_block_val.clear();
    cur_key_block_val.resize(spn.block_cnt);
    max_bias = 0;
    dfs_linear_key(0, p.first.first, p.first.second, io_pair);
    for (size_t i = 0; i < spn.block_cnt; i++) {
      if (!block_done[i] && get_block(p.first.second, i, spn.sbox.bits) != 0) {
        key_block_val_cnt[i][best_key_block_val[i]]++;
        if (key_block_val_cnt[i][best_key_block_val[i]] == num_cover) {
          INFO("Found best key of block " << dec << i << ", value = "
              << formatted_hex(spn.sbox.bits, true) << best_key_block_val[i]);
          key_block_val[i] = best_key_block_val[i];
          block_done[i] = true;
        }
      }
    }
  }
}

void diff_attack(int r) {
  if (all_of(block_done.begin(), block_done.end(), [](bool x){return x;})) {
    INFO("All keys already found, skipping differential attack.");
    return;
  }
  INFO("Trying differential attack.");
  INFO("Finding high probability paths.");
  paths.clear();
  dfs(0, 0, r - 1, 0, 0, 1, min_path_diff_prob,
      diff_tbl, diff_cands, bad_diff_block);
  INFO(paths.size() << " paths found.");
  sort(paths.begin(), paths.end(), cmp_path);
  for (auto& p : paths) {
    bool flag = false;
    for (size_t i = 0; i < spn.block_cnt; i++) {
      if (get_block(p.first.second, i, spn.sbox.bits) != 0) {
        if (!block_done[i]) flag = true;
      }
    }
    if (!flag) continue;
    uint num = diff_coef / p.second / p.second + 1;
    INFO("Using path " << formatted_hex(spn.total_bits, true) << p.first.first
        << " -> " << formatted_hex(spn.total_bits, true) << p.first.second
        << " with probability " << p.second << endl
        << "  Would choose " << dec << num << " plaintext pairs.");
    vector<pair<u64, u64>> io_pair = rand_iop(num, p.first.first);
    vector<pair<u64, u64>> o_pair;
    for (size_t i = 0; i < io_pair.size(); i += 2) {
      u64 y0 = io_pair[i].second, y1 = io_pair[i + 1].second;
      reverse(r, y0);
      reverse(r, y1);
      // only output is important in diff attack.
      // use output that is good on should-be-same block.
      bool good = true;
      for (uint j = 0; j < spn.block_cnt; j++) {
        if (get_block(p.first.second, j, spn.sbox.bits) == 0) {
          if (get_block(y0, j, spn.sbox.bits)
              != get_block(y1, j, spn.sbox.bits)) {
            good = false; break;
          }
        }
      }
      if (good) o_pair.emplace_back(y0, y1);
    }
    cur_key_block_val.clear();
    cur_key_block_val.resize(spn.block_cnt);
    max_prob = 0;
    dfs_diff_key(0, p.first.second, o_pair);
    for (size_t i = 0; i < spn.block_cnt; i++) {
      if (!block_done[i] && get_block(p.first.second, i, spn.sbox.bits) != 0) {
        key_block_val_cnt[i][best_key_block_val[i]]++;
        if (key_block_val_cnt[i][best_key_block_val[i]] == num_cover) {
          INFO("Found best key of block " << dec << i << ", value = "
              << formatted_hex(spn.sbox.bits, true) << best_key_block_val[i]);
          key_block_val[i] = best_key_block_val[i];
          block_done[i] = true;
        }
      }
    }
  }
}

int main(int argc, char* argv[]) {
  clock_t start_t = clock();
  parse_options(argc, argv);
  gen_linear_table();
  gen_diff_table();
  gen_bad_blocks();
  found_keys.resize(spn.rounds + 1);
  int cur_round = spn.rounds;
  bool all_done = true;
  while (all_done && cur_round > 1) {
    INFO("Trying to attack round " << cur_round);
    block_done.clear(); block_done.resize(spn.block_cnt);
    key_block_val.clear(); key_block_val.resize(spn.block_cnt);
    key_block_val_cnt.clear(); key_block_val_cnt.resize(spn.block_cnt);
    linear_attack(cur_round);
    diff_attack(cur_round);
    for (size_t i = 0; i < spn.block_cnt; i++) {
      if (block_done[i]) continue;
      if (key_block_val_cnt[i].empty()) continue;
      u32 best_key = 0;
      uint best_key_cnt = 0;
      for (auto& it : key_block_val_cnt[i]) {
        if (it.second > best_key_cnt) {
          best_key_cnt = it.second;
          best_key = it.first;
        }
      }
      block_done[i] = true;
      key_block_val[i] = best_key;
    }
    all_done = true;
    for (size_t i = 0; i < spn.block_cnt; i++) {
      all_done &= block_done[i];
      if (block_done[i]) {
        found_keys[cur_round] |= key_block_val[i] << (i * spn.sbox.bits);
      }
    }
    if (all_done) {
      if (cur_round != int(spn.rounds)) {
        found_keys[cur_round] = bit_permutation(found_keys[cur_round], spn.pbox.p);
      }
      INFO("Key for round " << dec << cur_round << " = "
          << formatted_hex(spn.total_bits, true) << found_keys[cur_round]);
      cout << dec << cur_round << ' '
        << formatted_hex(spn.total_bits, true) << found_keys[cur_round] << endl;
    } else {
      string ck(spn.total_bits, '?');
      for (size_t i = 0; i < spn.total_bits; i++) {
        if (block_done[i / spn.sbox.bits]) {
          size_t ii = i;
          if (cur_round != int(spn.rounds)) ii = spn.pbox[ii];
          ck[ii] = char('0' + get_bit(found_keys[cur_round], i));
        }
      }
      reverse(ck.begin(), ck.end());
      INFO("Recovered key: 0b" << ck);
      cout << dec << cur_round << " 0b" << ck << endl;
    }
    cur_round--;
  }
  if (all_done && cur_round == 1) {
    INFO("Trying to attack round 1");
    if (all_io.size() < num_io_r1) {
      vector<u64> inputs;
      for (uint i = 0; i < num_io_r1 - all_io.size(); i++) {
        inputs.push_back(rand_input());
      }
      blackbox(inputs);
    }
    // Just use all_io for round 1.
    vector<pair<u64, u64>> io_pairs = all_io;
    for (auto& p : io_pairs) {
      reverse(1, p.second);
    }
    u32 maxv = spn.sbox.max_input();
    u64 fk0 = 0, fk1 = 0;
    for (uint b = 0; b < spn.block_cnt; b++) {
      bool hasg = false;
      for (u32 k0 = 0; k0 <= maxv; k0++) {
        u32 k1 = 0;
        bool good = true;
        for (size_t i = 0; i < io_pairs.size(); i++) {
          auto& p = io_pairs[i];
          u32 block = get_block(p.first, b, spn.sbox.bits);
          u32 oblock = get_block(p.second, b, spn.sbox.bits);
          u32 pk1 = oblock ^ spn.sbox[block ^ k0];
          if (i == 0) k1 = pk1;
          else if (k1 != pk1) {
            good = false; break;
          }
        }
        if (good) {
          fk0 |= k0 << (b * spn.sbox.bits);
          fk1 |= k1 << (b * spn.sbox.bits);
          hasg = true;
          break;
        }
      }
      ASSERT(hasg, "Can't find key 0 / 1...");
    }
    found_keys[0] = fk0;
    found_keys[1] = bit_permutation(fk1, spn.pbox.p);
    INFO("Key for round 1 = "
        << formatted_hex(spn.total_bits, true) << found_keys[1]);
    INFO("Key for round 0 = "
        << formatted_hex(spn.total_bits, true) << found_keys[0]);
    cout << "1 " << formatted_hex(spn.total_bits, true) << found_keys[1] << endl;
    cout << "0 " << formatted_hex(spn.total_bits, true) << found_keys[0] << endl;
    // Verify keys are good.
    if (all_of(all_io.begin(), all_io.end(),
          [](pair<u64, u64>& p){ return spn.encrypt(p.first, found_keys) == p.second; })) {
      INFO("Found keys verified correct!");
    } else {
      ERR("Found keys incorrect...?");
    }
  }
  cerr << "plaintext-ciphertext pair: " << dec << all_io.size() << '.' << endl;
  cerr << "time: " << (clock() - start_t) / (double)CLOCKS_PER_SEC << " seconds." << endl;
}
// vim: fdm=marker:commentstring=\ \"\ %s:nowrap:autoread
