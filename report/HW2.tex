\documentclass[10pt,a4paper]{article}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{wasysym}
\usepackage{minted}
\usepackage{upquote}
\usepackage[margin=2.5cm]{geometry}
\usepackage{xeCJK}
\usepackage{titlesec}
\setCJKmainfont{WenQuanYi Micro Hei}
\XeTeXlinebreaklocale "zh"
\XeTeXlinebreakskip = 0pt plus 1pt
\setlength{\parskip}{0.6cm}
\linespread{1.2}\selectfont
\usepackage{enumerate}
\usepackage[T1]{fontenc}
\usepackage{concmath}
\usepackage{calc}
\titlespacing{\subsection}{0pt}{4pt plus 1pt minus 1pt}{0pt plus 1pt minus 1pt}

\newcommand{\x}{\item}
\newcommand{\be}{\begin{enumerate}[(a)]}
\newcommand{\ee}{\end{enumerate}}
\newcommand{\erf}{\operatorname{erf}}
\newcommand{\erfc}{\operatorname{erfc}}
\newcommand{\sign}{\operatorname{sign}}
\newcommand{\fal}[1]{\parbox[t]{\textwidth-4em}{\vspace{-2.4em}#1}}
\newcommand{\fm}[1]{\parbox[t]{\textwidth-4em}{\vspace{-1.5em}#1}}
\newcommand{\lcm}{\text{lcm}}

\begin{document}
\title{{\fontspec{Copperplate Gothic Bold}Topics in Cryptography Homework 2}}
\author{{\it{R02922059}} {時丕勳}}
\date{November 25, 2014}
\maketitle
All code at: \verb|https://bitbucket.org/peter50216/crypto-hw2/src|.

For this homework, I implemented two things: a differential attack on FEAL-4, and a general linear / differential attack on the toy Substitution-Permutation Network (SPN) taught in class.

\section{Differential attack on FEAL-4}
The code is in directory \verb|/feal/|. Type \verb|make| in the directory to make all executables.

The attack is based on content of \verb|http://www.theamazingking.com/crypto-feal.php|, all codes are written by myself.

Here's a simple explanation of every file:\\
{\ttfamily\bfseries feal.cpp}: FEAL-4 encryption implementation.\\
{\ttfamily\bfseries crack.cpp}: Differential attack on FEAL-4 encryption.\\
{\ttfamily\bfseries rand\_key}: Simple script to generate random round keys for FEAL-4. See sample usage in appendix.\\
{\ttfamily\bfseries blackbox}: Simple script to wrap \verb|./feal| and \verb|key|. See sample usage in appendix.\\
{\ttfamily\bfseries feal.h, util.h, Makefile, .gitignore}: Stuffs.

Most of the output (except found key) are default hidden, to see more verbose output, add \verb|-v| flag.

I used openmp to speed up the attack process. With a 16-core machine, it finds all round keys in about 17 seconds.

An interesting thing is that the key found is usually not exactly the same as key used. This is because the s-box of FEAL-4 has some too good differential property that always holds. So there are in fact some equivalent set of subkeys, that would always encrypt to same output. This actually cause a speed up of 4 times, since we can skip some subkeys when enumerating all subkeys.

\section{Differential / Linear attack on SPN}
The code is in directory \verb|/spn/|. Type \verb|make| in the directory to make all executables.

The attack is based on the powerpoint \verb|Intro_LC_DC.ppt| in class. I've used a C++ JSON parsing library (https://github.com/open-source-parsers/jsoncpp), all other codes are written by myself.

Here's a simple explanation of every file:\\
{\ttfamily\bfseries spn.cpp}: SPN encryption implementation.\\
{\ttfamily\bfseries crack.cpp}: Differential attack and linear attack on SPN encryption.\\
{\ttfamily\bfseries example.json}: The SPN used as example in powerpoint.\\
{\ttfamily\bfseries rand\_spn}: Simple script to generate random substitution and permutation for SPN. See sample usage in appendix.\\
{\ttfamily\bfseries rand\_sbox\_16\_4}: Simple script to generate random substitution, and use same permutation as \verb|example.json| for SPN.\\
{\ttfamily\bfseries rand\_key}: Simple script to generate random round keys for SPN. See sample usage in appendix.\\
{\ttfamily\bfseries serpent\_sbox.json}: Some SPN using the sbox of serpent. See sample usage in appendix.\\
{\ttfamily\bfseries blackbox}: Simple script to wrap \verb|./spn|, \verb|example.json| and \verb|key|. See sample usage in appendix.\\
{\ttfamily\bfseries jsoncpp.cpp, json/}: JsonCpp library.
{\ttfamily\bfseries spn.h, util.h, Makefile, .gitignore}: Stuffs.

I implemented both linear attack and differential attack. They can be used together, and both would try to break ALL subkeys, not just the last round.

Most of the output (except found key) are default hidden, to see more verbose output, add \verb|-v| flag.

I tried the program on \verb|example.json|, and it successfully recovered all keys. I was a little surprised that the path in powerpoint of both linear and differential attack aren't the one with highest bias / probability.

Random SPN are almost always cracked pretty fast. Using differential attack seems to be faster, but needs more ciphertext-plaintext pairs. (Maybe this is just because of my default parameter selection.)

I also tried the program using sbox from serpent. They have a pretty good linear and differential property, and can't be broken by default settings. BUT since the default round number are too small, if we add argument \verb|-L 0.01| to \verb|./crack| (that lowers the minimum bias of a path to be consider good), these can also be broken. But this means that it takes longer time and needs more known plaintext-ciphertext pairs for the attack to succeed.

To only use linear attack, add \verb|-d 2|. To only use differential attack, add \verb|-l 2|. See sample usage for more examples.

\section{Appendix}

\subsection{Program Arguments}
\subsubsection{FEAL-4}
\begin{minted}[frame=single]{text}
Usage: ./feal [options] [input 64bit numbers]...

Options:
    -k [keyfile]: File contains subkeys. (required)
    -v: verbose output
    -h: help message (this)
\end{minted}

\begin{minted}[frame=single]{text}
Usage: ./crack [options] [blackbox executable]

Options:
    -v: verbose output
    -h: help message (this)
    -n [num]: set per-round plain-text pair used (default 4)
              would use more if unlucky :D

Blackbox should be able to be executed as:
  blackbox [input 64bit integer]
  and output the encrypted result to stdout.
\end{minted}

\subsubsection{SPN}
\begin{minted}[frame=single]{text}
Usage: ./spn [options] [input numbers]...
If input numbers are empty, then read input numbers from stdin.

Options:
    -c [SPN json]: File contains description of SPN. (required)
    -k [keyfile]: File contains subkeys. (required)
    -v: verbose output
    -h: help message (this)
\end{minted}

\begin{minted}[frame=single]{text}
Usage: ./crack [options] [blackbox executable]

Options:
    -c [SPN json]: File contains description of SPN. (required)
    -v: verbose output
    -h: help message (this)
    -r: number of times a key guess being maximum to be considered
        correct. (to make things more robust.) (default 4)
  Linear Cryptanalysis:
    -l [double]: minimum linear bias for an item in
                 linear approximation table to be considered
                 (default 0.25)
    -L [double]: minimum linear bias for a path
                 (default 0.04)
    -q [double]: number of query to blackbox would be q*b^{-2}
                 where b is bias. (default: 20)  Differential Cryptanalysis:
    -d [double]: minimum probability for an item in
                 difference distribution table to be considered
                 (default 0.25)
    -D [double]: minimum probability for a path
                 (default 0.01)
    -w [double]: number of pairs to blackbox would be w*p^{-2}
                 where p is probability of path. (default: 4)
    -k [int]: number of query to blackbox for round 1. (default 30)

Blackbox should be able to read 64 bit integers from stdin
  and output the encrypted result to stdout.
\end{minted}

\subsection{Sample Usage}
\subsubsection{FEAL-4}

\begin{minted}[frame=single,label=Generate a random FEAL-4 key and break it]{text}
> ./rand_key > key && ./crack -v ./blackbox
Cracking subkey for round 3
Undo done, guessing subkey...
Progress: 0x00000000/0x80000000
...
Progress: 0x78000000/0x80000000
Found key for round 3 = 0x6e252f60
Cracking subkey for round 2
Undo done, guessing subkey...
Progress: 0x00000000/0x80000000
...
Progress: 0x78000000/0x80000000
Found key for round 2 = 0x01655f56
Cracking subkey for round 1
Undo done, guessing subkey...
Progress: 0x00000000/0x80000000
...
Progress: 0x78000000/0x80000000
There are 8 possible keys left. Use more diff pairs to find correct ones.
There are 4 possible keys left. Use more diff pairs to find correct ones.
There are 4 possible keys left. Use more diff pairs to find correct ones.
There are 4 possible keys left. Use more diff pairs to find correct ones.
Found key for round 1 = 0x2fee2a9f
Cracking subkey for round 0
Undo done, guessing subkey...
Progress: 0x00000000/0x80000000
...
Progress: 0x78000000/0x80000000
Found key for round 0 = 0x016c562b
Verifying the subkeys found...
Verify done! Here's the keys:
0x016c562b
0x2fee2a9f
0x01655f56
0x6e252f60
0xc7efe20a
0x422a8c4e
\end{minted}
\subsubsection{SPN}
\begin{minted}[frame=single,label=The example encryption in powerpoint]{bash}
> echo "0x3a94\n0xa94d\n0x94d6\n0x4d63\n0xd63f\n" > key
> ./spn -c example.json -k key 0x26b7
0xbcd6
\end{minted}
\begin{minted}[frame=single,label=Crack sample key and sample SPN]{text}
> echo "0x3a94\n0xa94d\n0x94d6\n0x4d63\n0xd63f\n" > key
> ./crack -c example.json ./blackbox -v
Reading SPN from file example.json
Generating linear approximation table.
Candidate for linear attack found: 0x0 -> 0x0 = 1
...
Candidate for linear attack found: 0xf -> 0xa = 0.5
Generating difference distribution table.
Candidate for diff attack found: 0x0 -> 0x0 = 1
...
Candidate for diff attack found: 0xf -> 0x9 = 0.25
Found too-linear block value: 0x1, b = 0xa
Found too-linear block value: 0x2, b = 0x2
Found too-linear block value: 0x3, b = 0x8
Found too-linear block value: 0x8, b = 0x8
Found too-linear block value: 0xb, b = 0x5
Trying to attack round 4
Trying linear attack on round 4
Finding high bias paths.
60 paths found.
Using path 0x00f0 -> 0xeee0 with bias 0.0791016
  Would choose 3197 plaintexts.
Using path 0x0090 -> 0xeeee with bias 0.0791016
  Would choose 3197 plaintexts.
Using path 0x00a0 -> 0x0eee with bias 0.0791016
  Would choose 3197 plaintexts.
Using path 0x00b0 -> 0x0eee with bias 0.0791016
  Would choose 3197 plaintexts.
Found best key of block 1, value = 0x3
Found best key of block 2, value = 0x6
Using path 0x00c0 -> 0xeee0 with bias 0.0791016
  Would choose 3197 plaintexts.
Using path 0x00d0 -> 0xeeee with bias 0.0791016
  Would choose 3197 plaintexts.
Found best key of block 0, value = 0xf
Found best key of block 3, value = 0xd
All keys already found, skipping differential attack.
Key for round 4 = 0xd63f
4 0xd63f
Trying to attack round 3
Trying linear attack on round 3
Finding high bias paths.
797 paths found.
Using path 0x0030 -> 0x9990 with bias 0.210938
  Would choose 450 plaintexts.
Using path 0x3000 -> 0x9999 with bias 0.210938
  Would choose 450 plaintexts.
Using path 0x0003 -> 0x0999 with bias 0.210938
  Would choose 450 plaintexts.
Using path 0x00a0 -> 0x4440 with bias 0.1875
  Would choose 569 plaintexts.
Found best key of block 1, value = 0x3
Found best key of block 2, value = 0xe
Using path 0x00b0 -> 0x4440 with bias 0.1875
  Would choose 569 plaintexts.
Found best key of block 3, value = 0x4
Using path 0xa000 -> 0x4444 with bias 0.1875
  Would choose 569 plaintexts.
Using path 0xb000 -> 0x4444 with bias 0.1875
  Would choose 569 plaintexts.
Found best key of block 0, value = 0x5
All keys already found, skipping differential attack.
Key for round 3 = 0x4d63
3 0x4d63
Trying to attack round 2
Trying linear attack on round 2
Finding high bias paths.
84517 paths found.
Using path 0x0300 -> 0x4004 with bias 0.375
  Would choose 143 plaintexts.
Using path 0x0800 -> 0x4444 with bias 0.375
  Would choose 143 plaintexts.
Using path 0x0100 -> 0x0444 with bias 0.375
  Would choose 143 plaintexts.
Using path 0x0200 -> 0x4440 with bias 0.375
  Would choose 143 plaintexts.
Using path 0x0830 -> 0x6446 with bias 0.28125
  Would choose 253 plaintexts.
Found best key of block 0, value = 0xa
Found best key of block 1, value = 0x1
Found best key of block 2, value = 0x7
Found best key of block 3, value = 0xa
All keys already found, skipping differential attack.
Key for round 2 = 0x94d6
2 0x94d6
Trying to attack round 1
Key for round 1 = 0xa94d
Key for round 0 = 0x3a94
1 0xa94d
0 0x3a94
Found keys verified correct!
plaintext-ciphertext pair: 3197.
time: 15.9533 seconds.
\end{minted}
(Not using \verb|-v| for the rest of example, since the output is too long...)
\begin{minted}[frame=single,label=Crack sample key and sample SPN (using differential attack)]{bash}
> echo "0x3a94\n0xa94d\n0x94d6\n0x4d63\n0xd63f\n" > key
> time ./crack -c example.json ./blackbox -l 2
4 0xd63f
3 0x4d63
2 0x94d6
1 0xa94d
0 0x3a94
plaintext-ciphertext pair: 193549.
time: 1.83882 seconds.
\end{minted}
\begin{minted}[frame=single,label=Trying to crack serpent's sbox with default setting]{bash}
> ./rand_key > key
> ./crack -c serpent_sbox.json "./spn -k key -c serpent_sbox.json"
4 0b????????????????
plaintext-ciphertext pair: 0.
time: 0.924978 seconds.
\end{minted}
\begin{minted}[frame=single,label=Crack serpent's sbox]{bash}
> ./rand_key > key
> ./crack -c serpent_sbox.json "./spn -k key -c serpent_sbox.json" -L 0.01
4 0x4987
3 0x5a7c
2 0x0aa5
1 0xcd78
0 0xc21b
plaintext-ciphertext pair: 20481.
time: 40.3899 seconds.
\end{minted}
\begin{minted}[frame=single,label=Crack random sbox]{bash}
> ./rand_key > key
> ./rand_sbox_16_4 > spn.json
>  ./crack -c spn.json "./spn -k key -c spn.json"
4 0x37e9
3 0xdac4
2 0x470e
1 0xaa97
0 0x6c7f
plaintext-ciphertext pair: 5121.
time: 3.06505 seconds.
\end{minted}
\begin{minted}[frame=single,label=Crack random SPN]{bash}
> ./rand_key > key
> ./rand_spn > spn.json
> ./crack -c spn.json "./spn -k key -c spn.json"
4 0x02ec
3 0x6148
2 0x51b2
1 0xb16d
0 0xb806
plaintext-ciphertext pair: 5121.
time: 2.34873 seconds.
\end{minted}
\begin{minted}[frame=single,label=Crack SPN with more round]{bash}
> ./rand_key 6 > key
> ./rand_spn 6 > spn.json
> ./crack -c spn.json "./spn -k key -c spn.json" -L 0.01
6 0x4b78
5 0x195c
4 0x6d3a
3 0x9ed6
2 0x34e5
1 0x24ea
0 0x465b
plaintext-ciphertext pair: 145636.
time: 38.0609 seconds.
\end{minted}
\begin{minted}[frame=single,label=Crack SPN with more bits]{bash}
> ./rand_key 4 24 > key
> ./rand_spn 4 4 24 > spn.json
> ./crack -c spn.json "./spn -k key -c spn.json"
4 0x246a1b
3 0xe782c8
2 0x9963c3
1 0x90a0e4
0 0x679acf
plaintext-ciphertext pair: 5121.
time: 53.9907 seconds.
\end{minted}
\begin{minted}[frame=single,label=Crack SPN with larger sbox]{bash}
> ./rand_key 4 20 > key
> ./rand_spn 4 5 20 > spn.json
> ./crack -c spn.json "./spn -k key -c spn.json" -l 0.1 -d 0.1
4 0x99a18
3 0xa3ee6
2 0x24300
1 0xc42ef
0 0x0e695
plaintext-ciphertext pair: 9103.
time: 23.9406 seconds.
\end{minted}

\end{document}
